# To run this server, run: ruby webserver.rb

require 'bundler'
require 'rubygems'
require 'sinatra'
require 'json'
require 'sinatra/cross_origin'
require_relative 'load_util'

Bundler.require()

set :environment, ENV['MODE']
set :bind, '0.0.0.0'

configure do
  enable :cross_origin
end

get "/" do
  return "<html> <h1> Calendar Hack a Fish Server </h1> </html>"
end

before do
  content_type('html')
end

before '/api/*' do
  content_type('application/json')
end

Dir[File.join(File.dirname(__FILE__), "routes", "*.rb")].each { |lib| require lib }
