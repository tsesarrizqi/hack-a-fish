post "/api/event" do
  body = JSON.parse(request.body.read)
  title = body['title']
  location = body['location']
  description = body['description']
  start_time = body['start_time']
  end_time = body['end_time']
  attendees = body['attendees']

  res = CreateEvent.new(title, location, {start: start_time, end: end_time}, attendees, {description: description, body: body}).perform

  meta = {title: title}
  if res[:success]
    status(200)
    return {
      message: "Successfully created an event",
      data: res[:data],
      meta: meta
    }.to_json
  else
    status(400)
    return {
      errors: {
        message: res[:error]
      },
      meta: meta
    }.to_json
  end
end
