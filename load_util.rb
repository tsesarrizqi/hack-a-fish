module LoadUtil
  require_relative 'lib/app'
  require_relative 'lib/modules'
  require_relative 'lib/services'
  require_relative 'lib/handlers'
end
