require 'google/apis/calendar_v3'
require 'googleauth'

module CalendarApi
  def self.credentials
    ::Google::Auth::DefaultCredentials.make_creds(
      scope: ['https://www.googleapis.com/auth/calendar','https://www.googleapis.com/auth/calendar.events'],
      json_key_io: File.open('config/calendar/service_account_key.json'))
  end

  def self.get_client
    ::Google::Apis::CalendarV3::CalendarService.new.tap{ |s| s.authorization = credentials }
  end
end
