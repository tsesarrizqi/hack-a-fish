class CreateEvent < Service
  def initialize(event_title, location, time, attendees, opts = {})
    puts opts[:body]
    @event_title = event_title
    @location = location
    @time = time
    @attendees = attendees
    @opts = opts
  end

  def perform
    calendar_id = calendar[:id] rescue nil
    return {success: false, error: "Calendar not exist. Create one first."} unless calendar_id

    event_data = {
      summary: @event_title,
      location: @location,
      description: @opts[:description] || @event_title,
      start: {
        date_time: parse_time(@time[:start]),
        time_zone: time_zone,
      },
      end: {
        date_time: parse_time(@time[:end]),
        time_zone: time_zone,
      },
      attendees: @attendees.map { |x| {email: x} },
      reminders: {
        use_default: false,
        overrides: [
          {reminder_method: 'email', minutes: 30},
          {reminder_method: 'popup', minutes: 30},
        ],
      },
      conference_data: {
        create_request: {
          request_id: "#{@event_title}-#{Time.now}",
        }
      }
    }

    event = Google::Apis::CalendarV3::Event.new(event_data)

    fields = "id,summary,location,description,start,end,attendees,conference_data"
    res =
      begin
        API.insert_event(calendar_id, event, fields: fields, send_updates: "all", conference_data_version: 1)
      rescue StandardError => e
        puts e
        nil
      end
    return {success: false, error: "Event creation failed"} unless res

    {
      success: true,
      data: {
        id: res.id,
        title: res.summary,
        location: res.location,
        description: res.description,
        start: {
          date_time: res.start.date_time,
          time_zone: res.start.time_zone,
        },
        end: {
          date_time: res.end.date_time,
          time_zone: res.end.time_zone,
        },
        attendees: res.attendees.map { |x| {name: x.display_name, email: x.email} },
        hangout_url: res.conference_data.entry_points[0].uri
      }
    }
  end

  def parse_time(time)
    time = "#{time}:00" if time.split(":").size < 3
    Time.parse(time).to_datetime.rfc3339
  end

  def time_zone
    @opts[:time_zone] || 'Asia/Jakarta'
  end
end
