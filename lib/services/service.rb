require 'yaml'
require 'json'

class Service < App
  API ||= CalendarApi.get_client
  CALENDAR_PATH ||= 'config/calendar/calendar.yml'

  def calendar
    @calendar ||= YAML.load_file(CALENDAR_PATH) rescue nil
  end
end
