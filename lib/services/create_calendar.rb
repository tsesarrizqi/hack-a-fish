require 'yaml'

class CreateCalendar < Service
  def initialize(calendar_title, opts = {})
    @calendar_title = calendar_title
    @opts = opts
  end

  def perform
    return {success: false, error: "Calendar exist. Id: #{calendar[:id]}."} if calendar

    calendar = Google::Apis::CalendarV3::Calendar.new(
      summary: @calendar_title,
      description: @opts[:description] || @calendar_title,
      time_zone: @opts[:time_zone] || 'Asia/Jakarta'
    )
    res = API.insert_calendar(calendar, fields: "id,summary,description,time_zone") rescue nil
    return {success: false, error: "Calendar creation failed"} unless res

    calendar = {
      id: res.id,
      title: res.summary,
      description: res.description,
      time_zone: res.time_zone
    }
    File.write(CALENDAR_PATH, calendar.to_yaml)
    {success: true, data: calendar}
  end
end
