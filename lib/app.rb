class App
  class Env
    class << self
      %w(development production).each do |e|
        define_method("#{e}?") { ENVIRONMENT == e.to_sym }
      end
    end
  end
end
